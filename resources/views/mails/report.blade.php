<div>
    <h1>License Report</h1>
    <p>Expired Licenses: {{ $expired }}</p>
    <p>Expired after 30 Days: {{ $near_expiration }}</p>
</div>