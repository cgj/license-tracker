@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if(count($errors))
                        @foreach($errors->all() as $e)
                            <div class="alert alert-danger">
                                <p>{{ $e }}</p>
                            </div>
                        @endforeach
                    @endif
                    <form action="{{ route('states.license.store', $state->id) }}" method="POST">
                        <div class="form-group">
                            <label for="state">State</label>
                            <p>{{ $state->name }}</p>
                        </div>
                        <div class="form-group">
                            <label for="number">License Number</label>
                            <input type="text" required name="number" class="form-control"
                                value="{{ old('number', $state->license ? $state->license->number : '') }}">
                        </div>
                        <div class="form-group">
                            <label for="number">License Expiration</label>
                            <input type="date" required name="expire_at" class="form-control"
                                value="{{ old('expire_at', $state->license ? $state->license->expire_at->format('Y-m-d') : now()->addMonth()->format("Y-m-d")) }}">
                        </div>
                        {{ csrf_field() }}
                        <button class="btn btn-primary">Save</button>
                        <a href="/home" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection