<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="panel panel-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>State</th>
                        <th>License Number</th>
                        <th>Expiration Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($states as $state)
                        @if($state->license)
                            <tr>
                                <td>{{ $state->name }}</td>
                                <td>{{ $state->license->number }}</td>
                                <td>{{ $state->license->expire_at->format("m/d/Y") }}</td>
                                <td>
                                    @if($state->license->expire_at->lt(now()))
                                        <span class="text-danger"><i class="fa fa-times-circle fa-fw"></i> License Invalid</span>
                                    @else
                                        <span class="text-success"><i class="fa fa-check-circle fa-fw"></i> License Valid</span>
                                    @endif
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>{{ $state->name }}</td>
                                <td><span class="text-danger">No License Found</span></td>
                                <td><span class="text-danger">No License Found</span></td>
                                <td><span class="text-danger"><i class="fa fa-times-circle fa-fw"></i> No License Found</span></td>
                            </tr>
                        @endif
                    @empty
                        <tr>
                            <td colspan="4" align="center">No State Found</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
