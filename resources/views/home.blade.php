@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <h1>Licenses</h1>
        </div>
        <div class="col-sm-6"><br>
            <form action="{{ route('report')}}" class="form-inline pull-right" method="POST">
                {{ csrf_field() }}
                <input type="email" name="email" required class="form-control">
                <button class="btn btn-primary">Send Report</button>
            </form>
        </div>
        <div class="col-sm-12">
            <div class="panel panel-default">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>State</th>
                            <th>License Number</th>
                            <th>Expiration Date</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($states as $state)
                            @if($state->license)
                                <tr>
                                    <td><a href="{{ route('states.license.index', $state->id) }}">{{ $state->name }}</a></td>
                                    <td>{{ $state->license->number }}</td>
                                    <td>{{ $state->license->expire_at->format("m/d/Y") }}</td>
                                    <td>
                                        @if($state->license->expire_at->lt(now()))
                                            <span class="text-danger"><i class="fa fa-times-circle fa-fw"></i> License Invalid</span>
                                        @else
                                            <span class="text-success"><i class="fa fa-check-circle fa-fw"></i> License Valid</span>
                                        @endif
                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td><a href="{{ route('states.license.index', $state->id) }}">{{ $state->name }}</a></td>
                                    <td><span class="text-danger">No License Found</span></td>
                                    <td><span class="text-danger">No License Found</span></td>
                                    <td><span class="text-danger"><i class="fa fa-times-circle fa-fw"></i> No License Found</span></td>
                                </tr>
                            @endif
                        @empty
                            <tr>
                                <td colspan="4" align="center">No State Found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
