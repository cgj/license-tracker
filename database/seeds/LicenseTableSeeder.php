<?php

use App\License;
use App\State;
use Illuminate\Database\Seeder;

class LicenseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = State::inRandomOrder()->limit(20)->get();
        $faker = Faker\Factory::create();
        License::truncate();
        foreach ($states as $state) {
            $state->license()->create([
                'number' => $faker->numberBetween(),
                'expire_at' => $faker->dateTime,
            ]);
        }        
    }
}
