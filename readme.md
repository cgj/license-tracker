# Installation Instruction
```
git clone https://gitlab.com/cgj/license-tracker.git
cd license-tracker
composer install
php artisan key:generate
php artisan migrate --seed
php artisan serve
```