<?php

namespace App\Console\Commands;

use App\License;
use App\User;
use Illuminate\Console\Command;

class GenerateReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate daily report of licenses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $license_count = License::where('created_at', '>', now()->subDays(30))->count();

        $users = User::get()->each(function ($user) use ($license_count) {
            $user->notify(new \App\Notifications\LicenseExpirationReport($license_count));
        });
    }
}
