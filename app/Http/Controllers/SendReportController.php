<?php

namespace App\Http\Controllers;

use App\License;
use App\Mail\GenerateReport;
use App\State;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendReportController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $licenses = License::get();
        $states = State::with('license')->get();

        $pdf = PDF::loadView('pdf.license', compact('states'))
                    ->save(storage_path().'/licenses/licenses.pdf');

        Mail::to($request->email)
            ->send(new GenerateReport($licenses));;

        return back();
    }
}
