<?php

namespace App\Http\Controllers;

use App\State;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StateLicensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($state)
    {
        $state = State::with('license')->find($state);
        return view('licenses.edit', compact('state'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $state)
    {
        $this->validate($request, [
            'number' => 'required|unique:licenses,number,' . $state . ',state_id',
            'expire_at' => 'required|date',
        ]);

        $state = State::with('license')->find($state);

        $date = Carbon::parse($request->expire_at);
        if ($state->license) {
            $state->license()->update([
                'number' => $request->number,
                'expire_at' => $date,
            ]);
        } else {
            $state->license()->create([
                'number' => $request->number,
                'expire_at' => $date,
            ]);
        }
        

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
