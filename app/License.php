<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    protected $dates = ['expire_at'];
    protected $fillable = [
        'number', 'expire_at'
    ];

    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
