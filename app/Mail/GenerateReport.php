<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenerateReport extends Mailable
{
    use Queueable, SerializesModels;

    public $license;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($license)
    {
        $this->license = $license;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $expired = $this->license->where('created_at', '<', now())->count();
        $near_expiration = $this->license->filter(function ($license) {
            return $license->created_at->between(now(), now()->addDays(30));
        })->count();
        return $this->view('mails.report')
                    ->with([
                        'expired' => $expired,
                        'near_expiration' => $near_expiration,
                    ])
                    ->attach(storage_path(). '/licenses/licenses.pdf');
    }
}
